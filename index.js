import express from 'express'
import resolvers from "./resolvers";
import schema from "./schema";

import {graphqlHTTP} from 'express-graphql'

var app = express()

app.get("/", (req, res) => {
    res.send("Un And Running With Grapqhql Course")
})

const root = resolvers

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
}))

app.listen(8082, () => {
    console.log('Running at 8082')
})