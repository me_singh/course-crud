# lco-graphql
A standard babel setup
Starter project setup for upcoming crash courses



#### Example of Query and Mutation supported

`mutation {
  createCourse(input: {
    courseName: "Course 3",
    price: 200,
    stack: MOBILE
    teachingAssists: [
      {
      	firstName: "Roshni",
      	lastName: "Chauhan",
      	experience: 4
      },
      {
        firstName: "Arun",
      	lastName: "Chauhan",
      	experience: 6
      }
    ]
  }) {
    id
    courseName
    price
    stack
    email
    teachingAssists {
      firstName
      experience
    }
  }
}`

`query {
  getCourse(id: "qlg_pou4doqlzh-xxYtm8") {
    id
    courseName
    teachingAssists {
      firstName
      experience
    }
  }
}`
