import {nanoid} from 'nanoid';

class Course {

    constructor(id, {
        courseName, category, price, language, email, stack, teachingAssists
    }) {
        this.id = id
        this.courseName = courseName
        this.category = category
        this.price = price
        this.language = language
        this.email = email
        this.stack = stack
        this.teachingAssists = teachingAssists
    };

}

const courseHolder = {}

const resolvers = {

    getCourse: ({ id }) => {
        console.log('getCourse: ', JSON.stringify(courseHolder))
        return new Course(id, courseHolder[id])
    },

    createCourse: ({ input }) => {
        let id = nanoid()
        courseHolder[id] = input
        console.log('createCourse: ', JSON.stringify(courseHolder))
        return new Course(id, input)
    }
}


export default resolvers;